# Библиотек компонентов компании Ditcase

[Ссылка на Storybook](https://ditcase.gitlab.io/crm-ui-kit/)

## Обновление
1) Авторизоваться в NPM `npm login`
2) Залить изменения в репозиторий
3) Увеличить версию в `package.json`
4) Запустить команду `npm run pub`
