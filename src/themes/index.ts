import { darkTheme } from './darkTheme'
import { defaultTheme } from './theme'

export {
	darkTheme,
	defaultTheme,
}
