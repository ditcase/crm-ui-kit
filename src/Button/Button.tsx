import React, { FC } from 'react'
import styled from 'styled-components'

import { BaseComponent } from '../BaseComponent'

export interface ButtonProps extends React.HTMLAttributes<HTMLButtonElement>{
	title: string
	theme?: 'default' | 'dark'
	textColor?: string
	bgColor?: string
}

export const Button: FC<ButtonProps> = ({
	theme = 'default',
  textColor,
	title,
	bgColor,
	...props
}) => {
	return (
		<BaseComponent theme={theme}>
			<StyledButton textColor={textColor} bgColor={bgColor} {...props}>
				{title}
			</StyledButton>
		</BaseComponent>
	)
}

interface StyledButtonProps {
	bgColor?: string
	textColor?: string
}

const StyledButton = styled.button<StyledButtonProps>(({
	bgColor,
  textColor,
	theme,
}) => ({
	backgroundColor: bgColor ?? theme.colors.greenbright,
	color: textColor ?? theme.colors.mainwhite,
	cursor: 'pointer',
	fontWeight: 'bold',
	padding: '10px 24px',
	borderRadius: 8,
	border: 'none',
}))