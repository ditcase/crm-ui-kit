import React from 'react'
import { ComponentMeta,ComponentStory } from '@storybook/react'

import { Button } from './Button'

export default {
	title: 'Micro/Button',
	component: Button,
} as ComponentMeta<typeof Button>

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />

export const Default = Template.bind({})
Default.args = {
	title: 'Button',
	onClick: () => console.log('click!'),
}

export const Primary = Template.bind({})
Primary.args = {
	textColor: 'white',
	title: 'Button',
	bgColor: 'blue',
	onClick: () => console.log('click!'),
}